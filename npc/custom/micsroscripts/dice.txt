-	script	Dice#announcer	1,{

OnInit:
disablenpc "prtevent";
hideonnpc "Dice#evnt1";
end;

OnClock0130:
OnClock0430:
OnClock0730:
OnClock1030:
OnClock1330:
OnClock1630:
OnClock1930:
OnClock2230:
hideonnpc "Dice#evnt1";
announce "Dice Manager: We are going to have a Dice event.",0;
sleep 10000;
announce "Dice Manager: For those who wants to join, Please proceed to Lighthalzen and enter the Warp Portal.",0;
sleep 10000;
announce "Dice Manager: After 1 Minute the Portal will close.",0;
sleep 10000;
announce "Dice Manager: So please go to Lighthalzen and enter the Warp Portal now if you want to join.",0;
enablenpc "prtevent";
initnpctimer;
end;

OnTimer30000:
announce "Dice Manager: Last 30 seconds.",0;
sleep 5000;
announce "Dice Manager: If you want to join please enter the Warp Portal at Lighthalzen.",0;
end;

OnTimer50000:
announce "Dice Manager: Last 10 seconds.",0;
end;

OnTimer55000:
announce "Dice Manager: 5.",0;
end;

OnTimer56000:
announce "Dice Manager: 4.",0;
end;

OnTimer57000:
announce "Dice Manager: 3.",0;
end;

OnTimer58000:
announce "Dice Manager: 2.",0;
end;

OnTimer59000:
announce "Dice Manager: 1.",0;
end;

OnTimer60000:
announce "Dice Manager: Time's up.",0;
end;

OnTimer61000:
disablenpc "prtevent";
donpcevent "Dice#evnt1::OnEnable";
stopnpctimer;
end;

OnTimer62000:
announce "Dice Manager: The next Dice event will begin after 2 hours.",0;
end;
}

//--------------------------------------------------

lighthalzen,159,99,0	warp	prtevent	2,2,quiz_01,204,90

//--------------------------------------------------

quiz_01,204,93,6	script	Dice#evnt1	715,{

//--------------------------------------------------

mes "[Dice]";
mes "Please tell me your name";
next;
input .@name$;
if(.@name$ != strcharinfo(0)) {
mes "[Dice]";
mes "Are you sure thats your character name?";
close;
}
mes "[Dice]";
mes "Congrats. You've won.";
close2;
announce "Dice Manager: "+.@name$+" won our Dice Event!",0;
getitem 7539,5;
hideonnpc "Dice#evnt1";
warp "SavePoint",0,0;
end;

OnEnable:
mapannounce "quiz_01","Dice Manager: We are about to start the Dice event.",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: But before we start the event here's how to play the game. . .",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: I'm only gonna say this once so read carefully.",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: I'm going to pick a number, 1 to 4. Then I'll do a count down from 5 to 0.",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: All you have to do is go to the box of the number you want.",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: Example, If I get the number of 4, All the players standing on numbers 1 to 3 will be warped back to town.",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: We will do it again and again until we only have 1 player left on the map.",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: Oh yeah! Before I forget you MUST GO INSIDE THE BOX, because if you're caught standing on the stairs you'll be automatically disqualified.",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: That's that. Now, let's start!",0;
sleep 10000;
goto L_Start;
end;

L_Start:
if(getmapusers("quiz_01") == 1) goto L_Champ;
if(getmapusers("quiz_01") == 0) goto L_None;
if(getmapusers("quiz_01") > 1) {
set $@number, rand(1,4);
sleep 10000;
mapannounce "quiz_01","Dice Manager: I have a number now. Please go to the box of the number you want.... GO!",0;
sleep 10000;
mapannounce "quiz_01","Dice Manager: 5",0;
sleep 5000;
mapannounce "quiz_01","Dice Manager: 4",0;
sleep 4000;
mapannounce "quiz_01","Dice Manager: 3",0;
sleep 3000;
mapannounce "quiz_01","Dice Manager: 2",0;
sleep 2000;
mapannounce "quiz_01","Dice Manager: 1",0;
sleep 1000;
mapannounce "quiz_01","Dice Manager: Time's up.",0;
donpcevent "evnt#1::OnEnable";
mapannounce "quiz_01","Dice Manager: Winning number "+$@number+".",0;
if(($@number != 1) && ($@number != 2) && ($@number != 3)) goto L_Lose1;
if(($@number != 1) && ($@number != 2) && ($@number != 4)) goto L_Lose2;
if(($@number != 1) && ($@number != 3) && ($@number != 4)) goto L_Lose3;
if(($@number != 2) && ($@number != 3) && ($@number != 4)) goto L_Lose4;
end;
}

L_Lose1:
areawarp "quiz_01",183,81,191,60,"lighthalzen",158,92;
areawarp "quiz_01",195,81,203,60,"lighthalzen",158,92;
areawarp "quiz_01",207,81,215,60,"lighthalzen",158,92;
goto L_Start;
end;

L_Lose2:
areawarp "quiz_01",183,81,191,60,"lighthalzen",158,92;
areawarp "quiz_01",195,81,203,60,"lighthalzen",158,92;
areawarp "quiz_01",219,81,227,60,"lighthalzen",158,92;
goto L_Start;
end;

L_Lose3:
areawarp "quiz_01",183,81,191,60,"lighthalzen",158,92;
areawarp "quiz_01",207,81,215,60,"lighthalzen",158,92;
areawarp "quiz_01",219,81,227,60,"lighthalzen",158,92;
goto L_Start;
end;

L_Lose4:
areawarp "quiz_01",195,81,203,60,"lighthalzen",158,92;
areawarp "quiz_01",207,81,215,60,"lighthalzen",158,92;
areawarp "quiz_01",219,81,227,60,"lighthalzen",158,92;
goto L_Start;
end;

L_Champ:
mapannounce "quiz_01","Dice Manager: Come to the middle and tell me your name.",0;
hideoffnpc "Dice#evnt1";
end;

L_None:
hideonnpc "Dice#evnt1";
end;
}

-	script	evnt#1	-1,{

OnEnable:
areawarp "quiz_01",182,94,228,88,"lighthalzen",158,92;
areawarp "quiz_01",185,87,188,82,"lighthalzen",158,92;
areawarp "quiz_01",197,87,200,82,"lighthalzen",158,92;
areawarp "quiz_01",209,87,212,82,"lighthalzen",158,92;
areawarp "quiz_01",221,87,224,82,"lighthalzen",158,92;
end;
}

quiz_01	mapflag	nowarp
quiz_01	mapflag	nowarpto
quiz_01	mapflag	noteleport
quiz_01	mapflag	nosave
quiz_01	mapflag	nomemo
