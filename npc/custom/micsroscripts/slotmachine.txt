comodo,195,166,4	script	Slot Machine	563,{

	if( getgmlevel() == 99 ) {
		mes "Welcome Administrator.","What would you like to do?";
		menu "Play Game",-,"Change Slot Machine Mode",iMode;
		next;
		}
switch( getd(".mode"+strnpcinfo(3)+"") ){
	case 0: // Single Slot machine mode.
		mes "Do you want to play a game?";
		if( !.payment ) { mes "It costs: "+ .ssm_payment_message$[0] +" to play."; }
			else if ( .payment == 1 ) { mes "It costs: "+ .ssm_payment_message$[1] +" to play."; }
				else if ( .payment == 2 ) { mes "It costs: "+ .ssm_payment_message$[0] +" & "+ .payment_message$[1] +" to play."; }
		if( select("YES:NO") == 2 || Zeny < .ssm_payment[0] && ( !.payment || .payment == 2 ) || countitem(.ssm_payment[1]) < .ssm_payment[2] && .payment ){ close; }
		while( @menu == 1 ){
		if (.EventON == 0) {mes "Slot Machine already finished"; cutin "",255; end;}
			if( !.payment || .payment == 2 ) { Zeny -= .ssm_payment[0]; }
			if( .payment ) { delitem( .ssm_payment[1], .ssm_payment[2] ); }
			if( .soundeffects ) { soundeffect "se_cash_provider.wav",0; }
			.@a = rand(1,100);
			if( .@a < atoi(.ssm_animate$[0]) ){ .@a = 1; } else { .@a = 2; }
			.@b = 1;
			while( .@b < atoi(.ssm_animate$[.@a]) ) {
				cutin .ssm_animate$[3] + .@b,4; sleep2 ( ( atoi(.ssm_animate$[4]) * 1000 ) / atoi(.ssm_animate$[.@a]) ); .@b++;
				}
			if( .@a == 1 ){
				cutin .ssm_animate$[3] + atoi(.ssm_animate$[.@a]),4;
				dispbottom "Failed";
				}
			else {
				cutin .ssm_animate$[3] + atoi(.ssm_animate$[.@a]),4;
				switch(rand(9)) {
				case 0:
					getitem 603,5;
					break;
				case 1:
					getitem 617,5;
					break;
				case 2:
					getitem 644,5;
					break;
				case 3:
					getitem 616,5;
					break;
				case 4:
					getitem 7539,1;
					break;
				case 5:
					getitem 7539,5;
					break;
				case 6:
					getitem 7539,10;
					break;
				case 7:
					getitem 7539,15;
					break;
				case 8:
					getitem 7539,20;
					break;
				}
				}
			if( select("Another Round:I'm done") == 2 || Zeny < .ssm_payment[0] && ( !.payment || .payment == 2 ) || countitem(.ssm_payment[1]) < .ssm_payment[2] && .payment ){ cutin "",255; close; }
			}
		end;

	case 1: // Triple Slot machine mode.
		mes "Do you want to play a game?";
		if( !.payment ) { mes "It costs: "+ .tsm_payment_message$[0] +" to play."; }
			else if ( .payment == 1 ) { mes "It costs: "+ .tsm_payment_message$[1] +" to play."; }
				else if ( .payment == 2 ) { mes "It costs: "+ .tsm_payment_message$[0] +" & "+ .tsm_payment_message$[1] +" to play."; }
		if( select("YES:NO") == 2 || Zeny < .tsm_payment[0] && ( !.payment || .payment == 2 ) || countitem(.tsm_payment[1]) < .tsm_payment[2] && .payment ){ close; }
		while( @menu == 1 ){
		if (.EventON == 0) {mes "Slot Machine already finished"; cutin "",255; end;}
			if( !.payment || .payment == 2 ) { Zeny -= .tsm_payment[0]; }
			if( .payment ) { delitem( .tsm_payment[1], .tsm_payment[2] ); }
			if( .soundeffects ) { soundeffect "se_cash_provider.wav",0; }
			// Slot 1 = 100% Chance for success. (Because I didn't make a fail animation for it.
			.@2 = rand(1,100); //Rolls dice for Slot 2
			.@3 = rand(1,100); //Rolls dice for Slot 3
				if( .@2 <= atoi(.tsm_animate$[0]) && .@3 <= atoi(.tsm_animate$[1]) ){ .@a = 8; }
				else if( .@2 <= atoi(.tsm_animate$[0]) && .@3 > atoi(.tsm_animate$[1]) ){ .@a = 6; }
				else if( .@2 > atoi(.tsm_animate$[0]) && .@3 <= atoi(.tsm_animate$[1]) ){ .@a = 4; }
				else { .@a = 2; }
			.@b = 1;
			while( .@b < atoi(.tsm_animate$[.@a+1]) ) {
				cutin .tsm_animate$[.@a] + .@b,4; sleep2 ( ( atoi(.tsm_animate$[10]) * 1000 ) / atoi(.tsm_animate$[.@a+1]) ); .@b++;
				}
			cutin .tsm_animate$[.@a] + atoi(.tsm_animate$[.@a+1]),4;
			if( .@a == 2 ){
					switch(rand(13)) {
					case 0:
						getitem 11501,10;
						break;
					case 1:
						getitem 11502,10;
						break;
					case 2:
						getitem 13599,1;
						break;
					case 3:
						getitem 13607,1;
						break;
					case 4:
						getitem 12103,3;
						break;
					case 5:
						getitem 13582,1;
						break;
					case 6:
						getitem 7539,5;
						break;
					case 7:
						getitem 7539,5;
						break;
					case 8:
						getitem 7539,10;
						break;
					case 9:
						getitem 7539,10;
						break;
					case 10:
						getitem 7539,50;
						break;
					case 11:
						getitem 7539,100;
						break;
					case 12:
						getitem 5210,1;
						break;
				}
				} else { dispbottom "Failed"; }
			if( select("Another Round:I'm done") == 2 || Zeny < .tsm_payment[0] && ( !.payment || .payment == 2 ) || countitem(.tsm_payment[1]) < .tsm_payment[2] && .payment ){ cutin "",255; close; }
			}
		end;
	}

OnSingleSlot:
setd ".mode"+strnpcinfo(3)+"",0;
end;
OnTripleSlot:
setd ".mode"+strnpcinfo(3)+"",1;
end;

iMode:
next;
mes "Which did you want to do?";
menu "Change THIS machine's mode:Change ALL machine's mode",-;
if( @menu == 1 ) {
	next;
	mes "What mode would you like this machine to have?";
	menu "Single Slot Machine Mode:Triple Slot Machine Mode",-;
	if( @menu == 1 ) {
		setd ".mode"+strnpcinfo(3)+"",0;
		} else {
			setd ".mode"+strnpcinfo(3)+"",1;
			}
	close;
	} else {
		next;
		mes "What mode would you like to change all slot machines to?";
		menu "Single Slot Machine Mode:Triple Slot Machine Mode",-;
		if( @menu == 1 ) {
			donpcevent "::OnSingleSlot";
			} else {
				donpcevent "::OnTripleSlot";
				}
		close;
		}
OnClock0415:
OnClock0845:
OnClock1215:
OnClock1645:
OnClock2015:
OnClock0045:
	announce "Slot Machine will begin in 1 minute. Come warp to Comodo Theater!", bc_all|bc_blue;
	sleep 60000;
	announce "Come and play Slot Machine at Comodo Theater!", bc_all|bc_blue;
	set .EventON,1;
	hideoffnpc strnpcinfo(3);
	sleep 300000;
	announce "Slot Machine event already finished.", bc_all|bc_blue;
	set .EventON,0;
	hideonnpc strnpcinfo(3);
	end;

OnInit:
hideonnpc strnpcinfo(3);

//[ 0 = Single Slot Machine Mode ]_[ 1 = Triple Slot Machine Mode ]
setd ".mode"+strnpcinfo(3)+"",1;

//[0] = Fail Rate
//[1] = Fail (Do not change)
//[2] = Success (Do not change)
//[3] = File Name (Do not change)
//[4] = Animation Time (Do not change, for best results :D)
setarray .ssm_animate$[0],"30","29","33","slot_","3";

//[0] = Fail Rate "Slot 2"
//[1] = Fail Rate "Slot 3"
//[2] = SSS (Do not change)
//[3] = SSS_Count (Do not change)
//[4] = SSF (Do not change)
//[5] = SSF_Count (Do not change)
//[6] = SFS (Do not change)
//[7] = SFS_Count (Do not change)
//[8] = SFF (Do not change)
//[9] = SFF_Count (Do not change)
//[10] = Animation Time (Do not change, for best results :D)
setarray .tsm_animate$[0],"30","30","SSS_","41","SSF_","37","SFS_","41","SFF_","45","3";

// Payment Settings
// Payment Type
// 0 = Zeny, 1 = Item, 2 = Zeny&Item
.payment = 1;
// [0] = Zeny, [1] = Item ID, [2] = Item Amount;
setarray .ssm_payment[0],1000000,7539,5; // Single Slot Machine Payment Price
setarray .tsm_payment[0],1000000,7539,10; // Triple Slot Machine Payment Price

// DO NOT CHANGE BELOW
// Payment Text Syntax
setarray .ssm_payment_message$[0],""+ .ssm_payment[0] +" zeny",""+ getitemname(.ssm_payment[1]) +" x"+ .ssm_payment[2] +"";
setarray .tsm_payment_message$[0],""+ .tsm_payment[0] +" zeny",""+ getitemname(.tsm_payment[1]) +" x"+ .tsm_payment[2] +"";
end;
}
