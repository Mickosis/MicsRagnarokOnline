-	script	huntlist	-1,{
OnInit:
.Delay = 0;            // Quest delay, in hours (0 to disable).
.Quests = 5;            // Number of subquests per mission (increases rewards).
.Party = 3;             // Party options: 0 (exclude party kills), 1 (include party kills), 2 (same map only), 3 (screen area only)
.Reset = 250000;            // Reset options: -1 (abandoning mission sets delay time), 0 (no delay time), [Zeny] (cost to abandon mission, no delay time)
setarray .Count[0],     // Min and max monsters per subquest (increases rewards).
	10,30;
setarray .Modifier[0],  // Multipliers for Base Exp, Job Exp, and Zeny rewards.
	getbattleflag("base_exp_rate")/500,getbattleflag("job_exp_rate")/500,35;

bindatcmd("huntlist",strnpcinfo(3)+"::OnHuntingList",0);
end;

OnHuntingList:
if (Mission0) {
function Chk;
mes "[Mission Status]";
callsub Mission_Status;
close;
}
dispbottom "[Mission Status] You do not have any active Missions.";
end;


Mission_Status:
	@f = 0;
	deletearray .@j[0], getarraysize(.@j);
	for (.@i = 0; .@i < .Quests; .@i++) {
		.@j[.@i] = getd("Mission" + .@i);
		.@j[.Quests] = .@j[.Quests] + strmobinfo(3,.@j[.@i]);
		.@j[.Quests+1] = .@j[.Quests+1] + (strmobinfo(6,.@j[.@i]) / (getbattleflag("base_exp_rate") / 100) * .Modifier[0]);
		.@j[.Quests+2] = .@j[.Quests+2] + (strmobinfo(7,.@j[.@i]) / (getbattleflag("job_exp_rate") / 100) * .Modifier[1]);
		mes " > "+Chk(getd("Mission"+.@i+"_"),#Mission_Count) + strmobinfo(1,.@j[.@i]) + " (" + getd("Mission"+.@i+"_") + "/" + #Mission_Count + ")^000000";
	}
	close;

	function Chk {
		if (getarg(0) < getarg(1)) {
			@f = 1;
			return "^FF0000";
		} else
			return "^00FF00";
	}

}
